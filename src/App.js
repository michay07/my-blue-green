import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Test of revert</h1>
        <h2>Terraform</h2>
	    </header>
    </div>
  );
}

export default App;
